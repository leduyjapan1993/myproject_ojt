﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using OJT.Models;

namespace OJT.Data
{
    //https://docs.microsoft.com/en-us/aspnet/core/migration/identity?view=aspnetcore-3.1
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
              : base(options)
        {

        }
        public virtual DbSet<Categories> Categories { get; set; }
        public virtual DbSet<Products> Products { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Categories>().HasData(
                new Categories
                {
                    CategoriesId = 1,
                    CategoryName = "Kid"
                },
                new Categories
                {
                    CategoriesId = 2,
                    CategoryName = "Women & Men"
                });
            /*builder.Entity<Products>().HasData(
                new Products
                {
                    ProductsId = 1,
                    ProductName = "Áo mùa đông",
                    Price = 200000,
                    PhotoPath = "1.png",
                    CategoriesId = 1,
                    Detail = "Áo dành cho trẻ em từ 3-5 tuổi"
                },
                new Products
                {
                    ProductsId = 2,
                    ProductName = "Áo thun",
                    Price = 100000,
                    PhotoPath = "2.png",
                    CategoriesId = 2,
                    Detail = "Áo thun có đầy đủ các size"
                });*/
        }

    }
}
