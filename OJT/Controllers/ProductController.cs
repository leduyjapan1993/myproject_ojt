﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using OJT.Models;
using OJT.Services.Category;
using OJT.Services.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OJT.Controllers
{
    public class ProductController : Controller
    {
        private readonly ILogger<ProductController> _logger;
        private IProductService productService;
        private ICategoryService categoryService;
        public ProductController(ILogger<ProductController> logger, IProductService productService, ICategoryService categoryService)
        {
            _logger = logger;
            this.productService = productService;
            this.categoryService = categoryService;
        }

        public ActionResult Index()
        {
            var getCategory = categoryService.GetCategories();
            ViewData["Categories"] = new SelectList(getCategory, "CategoriesId", "CategoryName");
            return View();
        }
        [HttpPost]
        public ActionResult CreateProduct(Products products)
        {
            if (ModelState.IsValid)
            {

                return RedirectToAction("Index");
            }
            else
            {
                return PartialView("_CreateProduct", products);
            }
        }
    }
}
