﻿using OJT.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OJT.Services.Product
{
    public interface IProductService
    {
        bool CreateProduct(Products products);
    }
}
