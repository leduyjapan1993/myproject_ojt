﻿using OJT.Data;
using OJT.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OJT.Services.Category
{
    public class CategoryService : ICategoryService
    {
        private readonly ApplicationDbContext applicationDbContext;

        public CategoryService(ApplicationDbContext applicationDbContext)
        {

            this.applicationDbContext = applicationDbContext;
        }

        public IEnumerable<Categories> GetCategories()
        {
            return applicationDbContext.Categories;
        }
    }
}
