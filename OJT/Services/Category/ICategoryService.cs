﻿using System.Collections.Generic;
using OJT.Models;

namespace OJT.Services.Category
{
    public interface ICategoryService
    {
        IEnumerable<Categories> GetCategories();
    }
}
