﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OJT.Migrations
{
    public partial class SeedDbcategories : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "CategoriesId", "CategoryName" },
                values: new object[] { 1, "Kid" });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "CategoriesId", "CategoryName" },
                values: new object[] { 2, "Women & Men" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "CategoriesId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "CategoriesId",
                keyValue: 2);
        }
    }
}
