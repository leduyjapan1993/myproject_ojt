﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OJT.Models
{
    public class Orders
    {
        public int OrdersId { get; set; }
        public DateTime OrderDate { get; set; }
        public int Id { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
    }
}
