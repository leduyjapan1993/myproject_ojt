﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations; // Dùng để required các trường dữ liệu
using System.Linq;
using System.Threading.Tasks;

namespace OJT.Models
{
    public class ApplicationUser : IdentityUser
    {

        [MaxLength(100)]
        public string FullName { set; get; }

        [MaxLength(255)]
        public string Address { set; get; }

        [DataType(DataType.Date)]
        public DateTime? Birthday { set; get; }

    }
}
