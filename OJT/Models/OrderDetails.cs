﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OJT.Models
{
    public class OrderDetails
    {
        public int OrderDetailId { get; set; }
        public Orders Orders { get; set; }
        public Products Products { get; set; }
        public int Count { get; set; }
        public float ToTal { get; set; }
    }
}
