﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OJT.Models
{
    public class Products
    {
        public int ProductsId { get; set; }
        [Required]
        public string ProductName { get; set; }
        [Required]
        public float Price { get; set; }
        public string PhotoPath { get; set; }
        public string Detail { get; set; }
        public int CategoriesId { get; set; }
        public Categories Categories { get; set; }

    }
}
