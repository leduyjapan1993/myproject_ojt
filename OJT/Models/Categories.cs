﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OJT.Models
{
    public class Categories
    {
        public int CategoriesId { get; set; }
        public string CategoryName { get; set; }
        public ICollection<Products> Products { get; set; }
    }
}
